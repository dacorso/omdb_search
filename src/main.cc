#include <iostream>
#include <string>

#include "ParamParser.hpp"
#include "Controller.hpp"
#include "Config.hpp" ///arquivo gerado pelo cmake na pasta de saída do projeto (CMAKE_BINARY_DIR)

static void show_help ()
{
  std::cout << "omdb_search version " << OMDBSEARCH_VERSION_MAJOR << "." << OMDBSEARCH_VERSION_MINOR << std::endl;
  std::cout << "USAGE: omdb_search -s RESOURCE [-t movie|series|episode] [-k KEY]" << std::endl;
  std::cout << "Searches for RESOURCE on omdbapi.com and optionally filters for movie, series or episode." << std::endl <<
      "The website requires a free key in order to work properly. If none is provided via -k option, a default one set during cmake build is used." << std::endl;

  std::cout << std::endl;
  std::cout << "Detailed description:" << std::endl;
  std::cout << "\t -h, --help \t\t Display this help" << std::endl;
  std::cout << "\t -s, --search RESOURCE \t The item to be searched. MANDATORY!" << std::endl;
  std::cout << "\t -t, --type TYPE \t Optionally filters the search by type, where TYPE must be movie|series|episode" << std::endl;
  std::cout << "\t -k, --key KEY \t\t Provides a key in order to properly use the omdbapi.com. For details, http://www.omdbapi.com/apikey.aspx" << std::endl;
  std::cout << "WARNING: the default key might expire without notice." << std::endl;
}

int main (int argc, const char* argv[])
{
  int32_t ret = 0;
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();

  if (r == ParamParser::Result::SUCCESS)
  {
    const std::string& result = ControllerOMDBSearch::roda (
                              parser.getTitle(),
                              parser.getType(),
                              parser.getKey());

    if (!result.empty())
    {
      std::cout << result << std::endl;
    }
    else
    {
      std::cout << "Failure!" << std::endl;
      ret = 2;
    }
  }
  else
  {
    if (r != ParamParser::Result::SHOW_HELP)
    {
      ret = 1;
    }

    show_help ();
  }

  return ret;
}
