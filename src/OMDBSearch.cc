#include "OMDBSearch.hpp"

#include <algorithm>
#include "Config.hpp"


/**********************
 * Public session
 *********************/

OMDBSearch::OMDBSearch () :
                OMDBSearch ("http://www.omdbapi.com", 5000)
{

}

OMDBSearch::OMDBSearch  (const std::string& inUrl, uint32_t timeout_ms) :
            m_timeout_ms (timeout_ms)
{
  auto url = cpr::Url{inUrl};

  m_session.SetUrl (url);
  m_session.SetTimeout (cpr::Timeout(m_timeout_ms));
}

OMDBSearch::Result OMDBSearch::search (const std::string& title, const std::string& type, const std::string& key)
{
  cpr::Parameters params;

  if (!title.empty()) params.AddParameter (cpr::Parameter{"t", title});
  if (!type.empty())  params.AddParameter (cpr::Parameter{"type", type});
  if (!key.empty())   params.AddParameter (cpr::Parameter{"apikey", key});

  m_session.SetParameters (params);

  auto r = m_session.Get();

//  DEBUG_MSG ("url: " << r.url);
//  DEBUG_MSG ("http status: " << r.status_code);
//  DEBUG_MSG ("elapsed: " << r.elapsed);
//  DEBUG_MSG ("");

  m_search_result = r.text; //salva a resposta (se houve alguma)

  return validate_response (r);
}

const std::string& OMDBSearch::getLastSearch () const
{
  return m_search_result;
}

/**********************
 * Private session
 *********************/

OMDBSearch::Result OMDBSearch::validate_response (const cpr::Response& r) const
{
  OMDBSearch::Result result = OMDBSearch::Result::TITLE_NOT_FOUND;

  if (r.status_code != 0)
  {
    std::string response = get_value_from_key (r.text, "\"Response\"");

    if (response == "True")
    {
      result = OMDBSearch::Result::SUCCESS;
    }
    else if (response == "False")
    {
      std::string error = get_value_from_key (r.text, "\"Error\"");
      result =  error_code_from_string (error);
    }
    else result = OMDBSearch::Result::HTTP_ERROR; //provavelmente nem é o site correto
  }
  else if (r.error.code == cpr::ErrorCode::OPERATION_TIMEDOUT)
  {
    result = OMDBSearch::Result::TIMEOUT;
  }
  else
  {
    result = OMDBSearch::Result::HTTP_ERROR;
  }

  return result;
}

std::string OMDBSearch::get_value_from_key (const std::string& json, const std::string& key) const
{
  std::string value{};
  std::size_t pos = json.find (key);

  if (pos != std::string::npos)
  {
    auto jend  = json.end();
    auto colon = std::find (json.begin()+pos, jend, ':');
    auto begin = std::find ((colon+1), jend, '"');
    auto end   = std::find ((begin+1), jend, '"');

    if (begin != jend && end != jend)
    {
      value = std::string (begin+1, end);
    }
  }

  return value;
}

OMDBSearch::Result OMDBSearch::error_code_from_string (const std::string& error) const
{
  if (error == "Invalid API key!")     return OMDBSearch::Result::INVALID_KEY;
  if (error == "No API key provided.") return OMDBSearch::Result::NO_KEY;

  return OMDBSearch::Result::TITLE_NOT_FOUND;
}
