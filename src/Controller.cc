#include "Controller.hpp"

#include "OMDBSearch.hpp"
#include "FormatOutput.hpp"

#include "Config.hpp"

static std::string
err2String (OMDBSearch::Result err)
{
  switch (err)
  {
    case OMDBSearch::Result::SUCCESS:         return "SUCCESS!?";
    case OMDBSearch::Result::HTTP_ERROR:      return "HTTP error";
    case OMDBSearch::Result::INVALID_KEY:     return "OMDB key is invalid";
    case OMDBSearch::Result::NO_KEY:          return "OMDB needs a key";
    case OMDBSearch::Result::TIMEOUT:         return "Search timeout";
    case OMDBSearch::Result::TITLE_NOT_FOUND: return "Title not found";
  }

  return "Unknown error";
}

std::string ControllerOMDBSearch::roda (const std::string& title, const std::string& type, const std::string& key)
{
  OMDBSearch s;
  OMDBSearch::Result r = s.search (title, type, key.empty() ? OMDBSEARCH_DEFAULT_KEY : key);

  if (r == OMDBSearch::Result::SUCCESS)
  {
    FormatOutput f;
    return f.format (s.getLastSearch ());
  }
  else return err2String (r);
}
