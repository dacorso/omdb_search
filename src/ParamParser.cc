#include "ParamParser.hpp"

#include "Config.hpp"
#include "FormatHelper.hpp"

ParamParser::ParamParser (uint32_t argc, const char* argv[]) :
    m_result (Result::SHOW_HELP), m_cmdLine (argv+1, argv+argc)
{
}

std::string ParamParser::getCmdLine () const
{
  std::string ret;

  for (const std::string& str : m_cmdLine)
    ret += (str + " ");

  trim (ret);

  return ret;
}

std::string ParamParser::getTitle () const
{
  return m_title;
}

std::string ParamParser::getType () const
{
  return m_type;
}

std::string ParamParser::getKey () const
{
  return m_key;
}

bool ParamParser::extract_param (viterc& it, viterc end, std::string& param) const
{
  bool ret = true;

  param.clear ();

  while (it != end && (*it)[0] != '-')
  {
    param.append (*it);
    param.push_back (' ');
    ++it;
  }
  trim (param);

  return ret && !param.empty();
}

ParamParser::Result ParamParser::getResult() const
{
  return m_result;
}

ParamParser ParamParser::parse (uint32_t argc, const char* argv[])
{
  ParamParser pp {argc, argv};
  Result res = Result::SUCCESS;
  viterc it  = pp.m_cmdLine.begin();
  viterc end = pp.m_cmdLine.end();

  while (it != end)
  {
    if (it->compare ("-s") == 0 || it->compare ("--search") == 0)
    {
      if (!pp.extract_param (++it, end, pp.m_title))
      {
        res = Result::PARSER_FAILED;
        break;
      }
    }
    else if (it->compare ("-t") == 0 || it->compare ("--type") == 0)
    {
      if (!pp.extract_param (++it, end, pp.m_type))
      {
        res = Result::PARSER_FAILED;
        break;
      }
      if (pp.m_type != "movie" &&
          pp.m_type != "series" &&
          pp.m_type != "episode")
      {
        res = Result::INVALID_TYPE;
        break;
      }
    }
    else if (it->compare ("-k") == 0 || it->compare ("--key") == 0)
    {
      if (!pp.extract_param (++it, end, pp.m_key))
      {
        res = Result::PARSER_FAILED;
        break;
      }
    }
    else if (it->compare ("-h") == 0 || it->compare ("--help") == 0)
    {
      res = Result::SHOW_HELP;
      break;
    }
    else
    {
      res = Result::UNKNOWN_OPTION;
      break;
    }
  }

  /// só é sucesso caso não tenha dado erro && o campo 'título' foi preenchido
  pp.m_result = (res == Result::SUCCESS) ?
                    (pp.m_title.empty () ? Result::MISSING_SEARCH_PARAM : Result::SUCCESS) :
                    res;

  return pp;
}
