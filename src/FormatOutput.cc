#include "FormatOutput.hpp"

#include <cstdint>
#include <vector>
#include <algorithm>
#include <sstream>

#include "Config.hpp"
#include "FormatHelper.hpp"

/**********************
 * Public session
 *********************/

FormatOutput::FormatOutput (const std::string& json) :
              m_json (json)
{
}

std::string
FormatOutput::format ()
{
  std::string startMark{"{["};
  std::string endMark{"}]"};
  iterc  start = std::find_first_of (m_json.begin(), m_json.end(), startMark.begin(), startMark.end());
  riterc rend  = std::find_first_of (m_json.rbegin(), m_json.rend(), endMark.begin(), endMark.end());
  iterc end = (rend+1).base();

  if( (*start == '{' || *start == '[') &&
      (*end   == '}' || *end   == ']'))
    return parse_result (start, end);
  else
    return std::string ("not a json");
}

std::string
FormatOutput::format (const std::string& json)
{
  m_json = json;
  return format ();
}



/**********************
 * Private session
 *********************/

std::string FormatOutput::fix_ratings (const std::string& input)
{
  std::istringstream iss (input);
  std::string temp;

  std::getline (iss, temp);
  auto iter = std::find (temp.begin(), temp.end(), '-');
  std::string source (iter+2, temp.end());

  std::getline (iss, temp);
  iter = std::find (temp.begin(), temp.end(), '-');
  std::string value (iter+2, temp.end());

  return source + ": " + value + "\n";
}


bool FormatOutput::extract_field (iterc& cur, iterc& end, const std::string& mark, std::string& outfield)
{
  bool ret = true;

  auto pos = std::find_first_of (cur, end, mark.begin(), mark.end());

  if (pos != end)
  {
    outfield = std::string (cur, pos);
    unquote (outfield); //remove aspas, espaços e caracteres indesejados
  }
  else ret = false;

  cur = pos;

  return ret;
}

std::string FormatOutput::parse_result (iterc& cur, iterc end)
{
  std::ostringstream oss; //usado para construir a string de saída

  while (*cur != *end && cur != end)
  {
    std::string key;
    std::string value;

    //extrai key
    if (!extract_field (cur, end, ":]}", key) ||
        key.empty())
      break; //chave vazia; encerra o parser

    ++cur; //pula o ':'

    ///agora, temos o seguinte:
    ///ou a chave é simples (com ou sem aspas)
    ///ou é um objeto aninhado com subpares key:value
    ///ou é um array aninhado \t\n\r\f\v

    //encontra o primeiro caracter que não seja espaço, quebra de linha, etc
    cur = std::find_if (cur, end, [] (char c)  { return c != ' ' && c != '\n' && c != '\r' && c != '\t' && c != 'f' && c != '\v'; });

    if (*cur == '"')
    {
      extract_field (++cur, end, "\"", value);

      if (key != "Response") //pula o campo "Response":  "True"
        oss << key << " - " << value << std::endl;
      ++cur;
    }
    else if (*cur == '[')
    {
      oss << key << " -" << std::endl;

      while (*cur != ']')
      {
        std::string nested = parse_result (cur, end);
        if (!nested.empty())
        {
          ++cur;

          if (key == "Ratings")
            nested = fix_ratings (nested);

          oss << nested;
        }
        else break;
      }
      ++cur;
    }
    else if (*cur == '{')
    {
      std::string nested = parse_result (cur, end);
      ++cur;

      oss << key << " -" << std::endl;
      oss << nested;
    }
    else //chave simples sem aspas
    {
      extract_field (cur, end, ",}", value);
      oss << key << " - " << value << std::endl;
    }
  }

  return oss.str();
}
