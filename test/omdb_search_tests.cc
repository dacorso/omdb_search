#include "catch.hpp"

#include <cstdint>
#include <string>

#include "OMDBSearch.hpp"
#include "Config.hpp"

const std::string urlOMDB      {"http://www.omdbapi.com"};
const std::string urlGoogle    {"https://www.google.com"};
const std::string urlDNS       {"erradozzzz.com.br"};
const std::string urlNotFound  {"http://www.google.com/notfound"};
const std::string keyOK        {"d2c4a37e"};
const std::string keyBAD       {"zzzzaaaa"};
const std::string titleOK      {"Sherlock"};
const std::string titleBAD     {"Ziripoca"};
const int32_t timeout_ms       {5000};
const int32_t timeout_bad_ms   {10};

TEST_CASE("happy path")
{
  OMDBSearch s1(urlOMDB, timeout_ms);
  OMDBSearch s2; //default constructor

  CHECK(s1.search (titleOK, "series", keyOK) == OMDBSearch::Result::SUCCESS);
  CHECK(s1.search (titleOK, "movie", keyOK)  == OMDBSearch::Result::SUCCESS);
  CHECK(s2.search (titleOK, "movie", keyOK)  == OMDBSearch::Result::SUCCESS);
  CHECK(s1.getLastSearch() == s2.getLastSearch());
}

TEST_CASE("timeout")
{
  OMDBSearch s(urlOMDB, timeout_bad_ms);

  CHECK(s.search (titleOK, "", keyOK) == OMDBSearch::Result::TIMEOUT);
}

TEST_CASE("no key")
{
  OMDBSearch s(urlOMDB, timeout_ms);

  CHECK(s.search (titleOK, "", "") == OMDBSearch::Result::NO_KEY);
}

TEST_CASE("invalid key")
{
  OMDBSearch s(urlOMDB, timeout_ms);

  CHECK(s.search (titleOK, "", keyBAD) == OMDBSearch::Result::INVALID_KEY);
}

TEST_CASE("title not found")
{
  OMDBSearch s(urlOMDB, timeout_ms);

  CHECK(s.search (titleBAD, "", keyOK) == OMDBSearch::Result::TITLE_NOT_FOUND);
}

TEST_CASE("url valid, but not OMDB")
{
  OMDBSearch s(urlGoogle, timeout_ms);

  CHECK(s.search (titleOK, "", keyOK) == OMDBSearch::Result::HTTP_ERROR);
}

TEST_CASE("dns")
{
  OMDBSearch s(urlDNS, timeout_ms);

  CHECK(s.search (titleOK, "", keyOK) == OMDBSearch::Result::HTTP_ERROR);
}

TEST_CASE("url not found ")
{
  OMDBSearch s(urlNotFound, timeout_ms);

  CHECK(s.search (titleOK, "", keyOK) == OMDBSearch::Result::HTTP_ERROR);
}
