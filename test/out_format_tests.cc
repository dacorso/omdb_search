#include "catch.hpp"

#include <string>
#include <fstream>
#include <sstream>

#include "FormatOutput.hpp"
#include "TestConfig.hpp"
#include "Config.hpp"

TEST_CASE("ratings.json")
{
  std::ifstream in  (PROJ_PATH"/test/json/ratings.json");
  std::ifstream out (PROJ_PATH"/test/json/ratings.parsed");
  std::stringstream bufferIn;
  std::stringstream bufferOut;
  bufferIn  << in.rdbuf();
  bufferOut << out.rdbuf();

  FormatOutput fo;
  std::string result = fo.format (bufferIn.str());

  //DEBUG_MSG (buffer.str());
  //DEBUG_MSG (result);

  CHECK (result == bufferOut.str());
}


TEST_CASE("omdb.json")
{
  std::ifstream in  (PROJ_PATH"/test/json/omdb.json");
  std::ifstream out (PROJ_PATH"/test/json/omdb.parsed");
  std::stringstream bufferIn;
  std::stringstream bufferOut;
  bufferIn  << in.rdbuf();
  bufferOut << out.rdbuf();

  FormatOutput fo;
  std::string result = fo.format (bufferIn.str());

  //DEBUG_MSG (buffer.str());
  //DEBUG_MSG (result);

  CHECK (result == bufferOut.str());
}


TEST_CASE("search_instead_of_title.json")
{
  std::ifstream in  (PROJ_PATH"/test/json/search_instead_of_title.json");
  std::ifstream out (PROJ_PATH"/test/json/search_instead_of_title.parsed");
  std::stringstream bufferIn;
  std::stringstream bufferOut;
  bufferIn  << in.rdbuf();
  bufferOut << out.rdbuf();

  FormatOutput fo;
  std::string result = fo.format (bufferIn.str());

  //DEBUG_MSG (buffer.str());
  //DEBUG_MSG (result);

  CHECK (result == bufferOut.str());
}


TEST_CASE("empty_ratings.json")
{
  std::ifstream in  (PROJ_PATH"/test/json/empty_ratings.json");
  std::ifstream out (PROJ_PATH"/test/json/empty_ratings.parsed");
  std::stringstream bufferIn;
  std::stringstream bufferOut;
  bufferIn  << in.rdbuf();
  bufferOut << out.rdbuf();

  FormatOutput fo;
  std::string result = fo.format (bufferIn.str());

  //DEBUG_MSG (buffer.str());
  //DEBUG_MSG (result);

  CHECK (result == bufferOut.str());
}

TEST_CASE("nested_array.json")
{
  std::ifstream in  (PROJ_PATH"/test/json/nested_array.json");
  std::ifstream out (PROJ_PATH"/test/json/nested_array.parsed");
  std::stringstream bufferIn;
  std::stringstream bufferOut;
  bufferIn  << in.rdbuf();
  bufferOut << out.rdbuf();

  FormatOutput fo;
  std::string result = fo.format (bufferIn.str());

  //DEBUG_MSG (buffer.str());
  //DEBUG_MSG (result);

  CHECK (result == bufferOut.str());
}

TEST_CASE("nested_object.json")
{
  std::ifstream in  (PROJ_PATH"/test/json/nested_object.json");
  std::ifstream out (PROJ_PATH"/test/json/nested_object.parsed");
  std::stringstream bufferIn;
  std::stringstream bufferOut;
  bufferIn  << in.rdbuf();
  bufferOut << out.rdbuf();

  FormatOutput fo;
  std::string result = fo.format (bufferIn.str());

  //DEBUG_MSG (buffer.str());
  //DEBUG_MSG (result);

  CHECK (result == bufferOut.str());
}
