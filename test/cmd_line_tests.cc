#include "catch.hpp"

#include <string>
#include "ParamParser.hpp"
#include "Config.hpp"

template<typename T, std::size_t size>
std::size_t getArrLength(T(&)[size]) { return size; }

TEST_CASE("happy path")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  std::string key {"d2c4a37e"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s", title.c_str(),
    "-t", type.c_str(),
    "-k", key.c_str()
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::SUCCESS);
  CHECK(parser.getTitle() == title);
  CHECK(parser.getType() == type);
  CHECK(parser.getKey() == key);
}

TEST_CASE("happy path - long options")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  std::string key {"d2c4a37e"};
  const char* argv[] = 
  {
    "path_to_executable",
    "--search", title.c_str(),
    "--type", type.c_str(),
    "--key", key.c_str()
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::SUCCESS);
  CHECK(parser.getTitle() == title);
  CHECK(parser.getType() == type);
  CHECK(parser.getKey() == key);
}

TEST_CASE("happy path - mixed options")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  std::string key {"d2c4a37e"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s", title.c_str(),
    "--type", type.c_str(),
    "-k", key.c_str()
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::SUCCESS);
  CHECK(parser.getTitle() == title);
  CHECK(parser.getType() == type);
  CHECK(parser.getKey() == key);
}

TEST_CASE("parametros com espaço sem aspas")
{
  std::string type {"movie"};
  std::string key {"d2c4a37e"};
  const char* argv[] =
  {
    "path_to_executable",
    "-s", "Star", "Wars", //note que cada palava em Star Wars vai ocupar uma posicao em argv
    "--type", type.c_str(),
    "-k", key.c_str()
  };
  int argc = getArrLength(argv);

  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();

  CHECK(r == ParamParser::Result::SUCCESS);
}

TEST_CASE("no parameters")
{
  const char* argv[] = 
  {
    "path_to_executable",
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::MISSING_SEARCH_PARAM);
}

TEST_CASE("default key")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s", title.c_str(),
    "-t", type.c_str(),
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::SUCCESS);
  CHECK(parser.getKey() == OMDBSEARCH_DEFAULT_KEY);
}

TEST_CASE("missing search param")
{
  std::string type {"movie"};
  std::string key {"d2c4a37e"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-t", type.c_str(),
    "-k", key.c_str()
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::MISSING_SEARCH_PARAM);
}

/**
* Apenas os valores movie, series e episode são aceitos no campo --type.
* O parser aceita opções repetidas (ex.: vários -s ou -t ou -k) sem gerar erro, descartando as anteriores e salvando a última apenas
*/
TEST_CASE("repeated option & validate --type")
{
  std::string title {"Star Wars"};
  std::string type1 {"movie"};
  std::string type2 {"episode"};
  std::string type3 {"series"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s", title.c_str(),
    "--type", type1.c_str(),
    "--type", type2.c_str(),
    "--type", type3.c_str(),
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::SUCCESS);
  CHECK(parser.getTitle() == title);
  CHECK(parser.getType() == type3);
}

TEST_CASE("invalid --type value")
{
  std::string title {"Star Wars"};
  std::string type {"book"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s", title.c_str(),
    "--type", type.c_str(),
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::INVALID_TYPE);
}

TEST_CASE("unknown short option")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s", title.c_str(),
    "-y",
    "-t", type.c_str(),    
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::UNKNOWN_OPTION);
}

TEST_CASE("unknown long option")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s", title.c_str(),
    "--yeah", "param"
    "-t", type.c_str(),    
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::UNKNOWN_OPTION);
}

TEST_CASE("param alone")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  const char* argv[] = 
  {
    "path_to_executable",
    "alone",
    "-s", title.c_str(),
    "-t", type.c_str(),    
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::UNKNOWN_OPTION);
}

TEST_CASE("option without parameter")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s",
    "-t", type.c_str(),    
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::PARSER_FAILED);
}

TEST_CASE("show help short")
{
  const char* argv[] = 
  {
    "path_to_executable",
    "-h" 
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::SHOW_HELP);
}

TEST_CASE("show help long")
{
  const char* argv[] = 
  {
    "path_to_executable",
    "--help" 
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::SHOW_HELP);
}

TEST_CASE("show help with other params")
{
  std::string title {"Star Wars"};
  std::string type {"movie"};
  const char* argv[] = 
  {
    "path_to_executable",
    "-s", title.c_str(),
    "-t", type.c_str(),
    "-h"   
  };
  int argc = getArrLength(argv);
 
  ParamParser parser = ParamParser::parse (argc, argv);
  ParamParser::Result r = parser.getResult ();
  
  CHECK(r == ParamParser::Result::SHOW_HELP);
}
