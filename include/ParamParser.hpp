#ifndef PARAMPARSER_HPP_
#define PARAMPARSER_HPP_

#include <string>
#include <vector>
#include <cstdint>

using viter  = std::vector<std::string>::iterator;
using viterc = std::vector<std::string>::const_iterator;

/** Classe que realize o parser dos parametros da linha de comando
 *
 * Procura pelas entradas -s ou --search, -t ou --type, -k ou --key e -h ou --help
 * O título (ou seja, -s/--search) é obrigatório, mas os outros são opcionais, entretanto,
 * a ajuda (-h/--help) se sobrepõe a todos os outros, ou seja, se presente, exibe a ajuda e ignora o resto.
 * Caso haja mais de uma entrada do mesmo tipo (ex.: multiplos -s), apenas a ultima é considerada sem gerar erros
 */
class ParamParser
{
  public:

    enum class Result
    {
      UNKNOWN_OPTION,       //! usuario informou uma opcao estranha
      MISSING_SEARCH_PARAM, //! usuario nao forneceu algo para ser procurado
      PARSER_FAILED,        //! sintaxe incorreta
      INVALID_TYPE,         //! valor informado no --type não é movie|series|episode
      SHOW_HELP,            //! -h ou --help presente; interrompe o parser
      SUCCESS               //! tudo certo
    };

    static ParamParser parse (uint32_t argc, const char* argv[]);
    Result getResult() const;

    std::string getCmdLine () const;
    std::string getTitle () const;
    std::string getType () const;
    std::string getKey () const;

  private:
    /**
     * Construtor apenas guarda os parametros da linha de comando, mas não efetua nenhuma operação
     * \param argc  numero de elementos em argv
     * \param argv  array de parametros
     */
    ParamParser (uint32_t argc, const char* argv[]);
    ParamParser () = delete;

    /**
     * Extrai o parâmetro imediatamente após uma das opções -s, -t, -k, preenchendo a variável de saída
     * O iterador do array avança para o opção seguinte consumindo tudo que estiver no caminho.
     * \param[in,out] cur  posição atual dentro do array
     * \param[in] end      marca de fim de array
     * \param[out] param   o parâmetro extraído
     *
     * \return true se deu tudo certo, ou false caso contrário
     */
    bool extract_param (viterc& cur, viterc end, std::string& param) const;

    std::string m_title; //! guarda o titulo se o parser foi correto
    std::string m_type;  //! guarda o tipo se houver e se o parser for correto
    std::string m_key;   //! guarda a chave do site omdb.com se houver e se o parser for correto
    Result m_result;     //! resultado do parsing

    std::vector<std::string> m_cmdLine; //! array com cada elemento da linha de comando
};

#endif //PARAMPARSER_HPP_
