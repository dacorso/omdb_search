#ifndef CONTROLLER_OMDBSEARCH_HPP_
#define CONTROLLER_OMDBSEARCH_HPP_

#include <string>

/**
 * Classe de controle que gerencia a rotina (executa o use case) de acessar o omdbapi.com, fazer a busca solicitada
 * pelo usuário e formatar a saída no padrão estabelecido
 */
class ControllerOMDBSearch
{
  public:
    /**
     * Roda o caso de uso OMDBSearch
     *
     * \param title  Nome do título a ser procurado
     * \param type   Tipo do título (movie, series, episode)
     * \param key    Chave de acesso a omdbapi.com. Se nenhuma chave for fornecida, uma default é usada automaticamente
     */
    static std::string roda (const std::string& title, const std::string& type, const std::string& key);

  private:
    ControllerOMDBSearch () = delete;
};

#endif /* CONTROLLER_OMDBSEARCH_HPP_ */
