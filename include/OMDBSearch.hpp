#ifndef INCLUDE_OMDBSEARCH_HPP_
#define INCLUDE_OMDBSEARCH_HPP_

#include <string>
#include <tuple>

#include <cpr/cpr.h>

/**
 * Classe que acessa o site omdbapi.com e pega as informações solicitadas pelo usuário
 *
 * A request é construída com base nos parametros informados e o HTTP GET é feito usando a lib cpr.
 * O resultado esperado é um json contendo detalhes sobre o título pesquisado
 */
class OMDBSearch
{
public:

  enum class Result
  {
    SUCCESS = 0,     //! tudo certo!
    TIMEOUT,         //! sem resposta do servidor
    NO_KEY,          //! nenhuma chave fornecida
    INVALID_KEY,     //! a chave fornecida não está funcionando e provavelmente expirou
    TITLE_NOT_FOUND, //! OMDB não sabe que filme doido é esse
    HTTP_ERROR       //! diversos tipos de erro, como host resolution, not found (404), invalid url, etc
  };

  /**
   * Construtor default que preenche automaticamente a URL para http://www.omdbapi.com e seta um timeout de 5000 ms
   */
  OMDBSearch ();
  OMDBSearch (const std::string& url, uint32_t timeout_ms);

  /**
   * Constrói a request com os parametros informados e faz a busca no omdbapi.com
   * A função retorna se a operação foi bem sucedida ou não, de forma que o conteúdo de fato
   * da consulta em caso de sucesso deve ser obtido de \see getLastSearch()
   *
   * \param title  Nome do título a ser procurado
   * \param type   Tipo do título (movie, series, episode)
   * \param key    Chave de acesso a omdbapi.com
   *
   * \return  Código de erro com o resultado da operação
   */
  Result search (const std::string& title, const std::string& type, const std::string& key);

  /**
   * Retorna o conteúdo da última \see search()
   */
  const std::string& getLastSearch () const;

private:
  /**
   * Procura dentro do json uma key específica e extrai o value dela. Funciona apenas da values simples (não vale array nem objets)
   *
   * \param json  The json
   * \param key   A chave a ser procurada dentro do json
   *
   * \return  O value referente a key informada
   */
  std::string get_value_from_key (const std::string& json, const std::string& key) const;

  /**
   * Dada uma mensagem de erro do site omdbapi.com, retorna um código da enum OMDBSearch::Return
   *
   * \param error  A mensagem de erro
   *
   * \return  O código equivalente à mensagem
   */
  Result error_code_from_string (const std::string& error) const;

  /**
   * Pega o resultado do HTTP GET e verifica se tudo ocorreu bem
   *
   * \param r  O resultado do GET
   *
   * \return  Código de erro da enum OMDBSearch::Return
   */
  Result validate_response (const cpr::Response& r) const;

  cpr::Session m_session;       //! sessão da lib cpr que vai efetuar o GET
  std::string  m_search_result; //! a string resultado do GET
  uint32_t     m_timeout_ms;    //! timeout da operação informado no construtor
};



#endif /* INCLUDE_OMDBSEARCH_HPP_ */
