#ifndef INCLUDE_FORMAT_OUTPUT_HPP
#define INCLUDE_FORMAT_OUTPUT_HPP

#include <string>

using iter   = std::string::iterator;
using iterc  = std::string::const_iterator;
using riter  = std::string::reverse_iterator;
using riterc = std::string::const_reverse_iterator;

/*
 * Classe que formata o resultado da busca de acordo com o padrão que foi pré-estabelecido
 *
 */
class FormatOutput
{
public:
  FormatOutput () = default;
  FormatOutput (const std::string& json);

  /**
   * Formata a string fornecida no construtor
   *
   * \return  Uma string formatada no padrão combinado
   * \return  "not a json", se a entrada estiver vazia ou esquisita
   */
  std::string format ();


  /**
   * Formata a string fornecida de parametro
   *
   * \return  Uma string formatada no padrão combinado
   * \return  "not a json", se a entrada estiver vazia ou esquisita
   */
  std::string format (const std::string& json);

private:
  /**
   * Procura pelas marcas informadas e extrai a substring que está a esquerda da marca.
   * O iterator 'cur' é avançado para além da string extraída, ou seja, a string é consumida
   *
   * \param [in,out] cur    posição atual dentro da string
   * \param [in] end        posição final da string
   * \param [in] mark       o que está sendo procurado
   * \param [out] outfield  campo de saída contento a string extraída
   *
   * \return falso se não conseguiu extrair nada
   */
  static bool extract_field (iterc& cur, iterc& end, const std::string& mark, std::string& outfield);

  /*
   * Função auxiliar que formata os elementos do campo "Ratings"
   *
   * \param input  Os sub-elementos do campo Ratings
   *
   * \return Uma nova string contendo os elementos do campo Ratings formatados no padrão esperado
   */
  static std::string fix_ratings (const std::string& input);

  /*
   * Função que de fato consome a entrada json e formata a saída no padrão pré-estabelecido
   *
   * \param cur  Inicio da string
   * \param end  Fim da string
   *
   * \return Uma string formatada conforme o combinado
   */
  static std::string parse_result (iterc& cur, iterc end);

  std::string m_json; //!cópia da string a ser formatada
};

#endif //INCLUDE_FORMAT_OUTPUT_HPP
